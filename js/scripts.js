var app = app || {};

var spBreak = 767;

app.init = function () {

  app.toggleNavigation();
  app.initSlider();
  app.initBackToTop();
  app.lightbox();
  app.initFixedElement();

};

app.isMobile = function () {

  return window.matchMedia('(max-width: ' + spBreak + 'px)').matches;

};

/** Function Block Body
 */
app.stickBody = function () {
  $('body').css({
    'position':   'fixed',
    'top':        -offsetY + 'px',
    'width':      '100%'
  });
};

/** Function unBlock Body
 */
app.unStickBody = function () {
  $('body').css({
    'position':   'static',
    'top':        'auto',
    'width':      'auto'
  });
  $(window).scrollTop(offsetY);
};

/** Function Navigation Menu
 */
app.toggleNavigation = function () {

  var navDropdown   = $('.js-nav-dropdown-icon');
  var navBtn        = $('.navbar-toggler');
  var navCollapse   = $('.navbar-collapse');
  var navClose      = $('.js-close-nav');

  var funcInit = function () {

    if(navDropdown.length) {
      funcDropdownNav();
    }

    if(navBtn.length) {
      funcClickHamburger();
    }

    funcClose();

  };

  var funcDropdownNav = function () {

    navDropdown
      .off('click.btnClick')
      .on('click.btnClick', function () {
        $(this).toggleClass('is-active');
        $(this)
          .closest('.nav-item')
          .find('.nav-dropdown')
          .slideToggle(300);
      });

  };

  var funcClickHamburger = function () {

    navBtn
      .off('click.btn')
      .on('click.btn', function () {

        navCollapse.slideToggle();

        /* Add CSS - overflow: auto */
        if (navCollapse.hasClass('is-active')) {
          navCollapse.removeClass('is-active');
        } else {
          setTimeout(function () {
            navCollapse.addClass('is-active');
          }, 410);
        }

        $(this).toggleClass('is-active');
        $(this)
          .find('.toggler-btn')
          .toggleClass('is-active');
        $('body')
          .toggleClass('is-toggle-nav');

        /* Fixed Body */
        if ($('body.is-toggle-nav').length) {
          offsetY = window.pageYOffset;
          app.stickBody();
        } else {
          app.unStickBody();
        }

      });

  };

  var funcClose = function () {

    navClose.off('click.btn').on('click.btn', function () {
      navCollapse.slideUp();
      navCollapse.removeClass('is-active');
      $('.toggler-btn').removeClass('is-active');
      $('body').removeClass('is-toggle-nav');
      app.unStickBody();
    });

  };

  funcInit();

};

/** Function Slider
 */
app.initSlider = function () {

  var topKeyvisual = $('.js-top-keyvisual');
  if(topKeyvisual.length) {
    topKeyvisual
      .slick({
        infinite:         true,
        slidesToShow:     1,
        slidesToScroll:   1,
        autoplay:         true,
        speed:            1000,
        autoplaySpeed:    2000,
        arrows:           true,
        dots:             false
      });
  }

  var profileSlider = $('.js-profile-slider');
  if(profileSlider.length) {
    profileSlider
      .slick({
        infinite:         true,
        slidesToShow:     1,
        slidesToScroll:   1,
        autoplay:         true,
        speed:            1000,
        autoplaySpeed:    2000,
        arrows:           true,
        dots:             false,
        prevArrow:        '.js-arrow-prev',
        nextArrow:        '.js-arrow-next',
      });
  }

};

/** Function Back To Top
 */
app.initBackToTop = function () {

  var btnBack = $('.js-backtotop');

  var funcInit = function () {

    btnBack.off('click.btnClick').on('click.btnClick', function (event) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: 0
      }, 500);
    });

    funcFadeBtn();

    $(window).on('load scroll resize', function () {
      funcFadeBtn();
    });

  };

  var funcFadeBtn = function () {
    if ($(window).scrollTop() > $(window).height() * 0.2) {
      if (!btnBack.is(':visible')) {
        btnBack.css('opacity', 0).show();
        btnBack.animate({
          opacity: 1
        }, 400);
      }
    } else {
      if (btnBack.is(':visible') && !btnBack.is(':animated')) {
        btnBack.animate({
          opacity: 0
        }, 400, function () {
          btnBack.css('opacity', 1).hide();
        });
      }
    }
  };

  if(btnBack.length) {
    funcInit();
  }

};

/** Function Lightbox
 */
app.lightbox = function () {

  var lightbox = $('.lightbox');
  var lightboxBtn = $('.js-lightbox');

  var funcInit = function () {
    lightboxBtn.on('click', function (event) {
      event.preventDefault();
      var lightboxID = $(this).attr('data-lightbox');
      funcOpenLightbox(lightboxID);
    });
    lightbox.find('.js-close').click(function () {
      var lightboxTarget = $(this).parents('.lightbox');
      funcCloseLightbox(lightboxTarget);
    });

  };

  var funcOpenLightbox = function (lightboxID) {

    $('.lightbox[data-lightbox="' + lightboxID + '"]').fadeIn('fast', function () {
      offsetY = window.pageYOffset;
      app.stickBody();
      $(document).on('touchmove', function (e) {
        if( $(e.target).parents('.window').length == 0 ) return false;
      });
    });

  };

  var funcCloseLightbox = function (lightbox) {
    app.unStickBody();
    lightbox.fadeOut('fast');
    $(document).off('touchmove');
  };

  if(lightbox.length) {
    funcInit();
  }

};

/** Function FixedElement
 */
app.initFixedElement = function () {

  var fixed = $('.js-fixed');

  var funcInit = function () {
    funcActive();
    $(window).on('load scroll', function () {
      funcActive();
    });
  };

  var funcActive = function () {

    fixed.each(function () {
      var item = $(this);
      if (!$('.' + item.attr('data-end')).length) {
        return false;
      }
      var headerHeight = $('header').outerHeight() + 20;
      var itemHeading =   item.find('.item');
      var star =          item.offset().top - 100;
      var stop  =         $('.' + item.attr('data-end')).offset().top - item.find('.item').outerHeight();

      if($(window).scrollTop() < star) {
        itemHeading.css({
          'top': '0',
          'position': 'relative'
        });
      } else if ($(window).scrollTop() >= (star) && $(window).scrollTop() <= (stop - headerHeight)) {
        itemHeading.css({
          'top': headerHeight + 'px',
          'position': 'fixed'
        });
      } else if ($(window).scrollTop() > (stop - headerHeight)) {
        itemHeading.css({
          'top': stop + 'px',
          'position': 'absolute'
        });
      }

    });
  };

  if(fixed.length) {
    funcInit();
  }

};

$(function () {

  app.init();

});
